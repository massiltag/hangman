<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pseudo</title>

    <!-- jQuery -->
    <script src="./lib/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Our files -->
    <link rel="stylesheet" href="./styles.css">
    <script src="./script.js"></script>
</head>

<body onload="onLoad()" class="container">

<div style="text-align: center;"><h1>Le jeu du pendu</h1></div>

<div class="card gradient-card">
    <div class="card-body">
        <h3 class="card-title">Le jeu du pendu</h3>
        <p class="card-text">
            Tentez de deviner le mot secret en entrant des lettres une par une au clavier.
            Ne gaspillez pas vos coups, car si trop de vos choix sont erronés vous tuerez
            le pendu et vous perdrez la partie.
        </p>
    </div>
</div>

<div class="card gradient-card">
    <div class="card-body">
        <h3>
            Veuillez rentrer un nom
        </h3>

        <form action="index.php" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <label>Pseudo choisi</label>
                <input type="text" class="form-control" name="pseudo" placeholder="Rentrer un pseudonyme" required>
            </div>
            <button type="submit" class="btn btn-primary submit">Proposer</button>
        </form>

    </div>
</div>

<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>


</body>

</html>
