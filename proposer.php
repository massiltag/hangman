<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jeu</title>

    <!-- jQuery -->
    <script src="./lib/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Our files -->
    <link rel="stylesheet" href="./styles.css">
    <script src="./script.js"></script>

    <style>
        .result-img {
            display: block;
            margin: 20px auto 20px auto;
            border-radius: 10px;
            width: 40%;
        }
    </style>
</head>

<body onload="onLoad()" class="container">

    <div style="text-align: center;"><h1><?php echo $_SESSION["pseudo"] ?>, propose un mot !</h1></div>

    <div class="card gradient-card">
        <div class="card-body">
            <h3 class="card-title">Le jeu du pendu</h3>
            <p class="card-text">
                Tentez de deviner le mot secret en entrant des lettres une par une au clavier.
                Ne gaspillez pas vos coups, car si trop de vos choix sont erronés vous tuerez
                le pendu et vous perdrez la partie.
            </p>

            <form action="index.php" method="post" >
                <input type="hidden" name="action" value="quit">
                <button type="submit" style="margin-left: 4px" class="btn btn-primary submit">Se déconnecter</button>
            </form>

            <form action="index.php" method="post" >
                <input type="hidden" name="action" value="reset">
                <button type="submit" class="btn btn-primary submit">Réinitialiser</button>
            </form>
        </div>
    </div>

    <div class="card gradient-card" style="display: <?php echo ($redis->get('status') != 0) ? 'block' : 'none'?>">
        <div class="card-body">
            <h3>
                Résultat <br>
                <small class="text-muted"><?php echo ($redis->get('status') == -1) ? 'Dommage, vous êtes nul 🙁' : 'Bravo, vous êtes trop fort 😍'?></small>
            </h3>

            <?php
                if ($redis->get('status') != 0) {
                    $img_name = ($redis->get('status') == -1) ? "lose" : "win";

                    echo "<img class=\"result-img\" src=\"./img/" . $img_name . ".jpeg\"/>";
                }
            ?>
        </div>
    </div>


    <div class="card gradient-card">
        <div class="card-body">
            <h3>
                Proposer un mot <br>
                <small class="text-muted">Aucun mot n'a été choisi pour l'instant. Vous pouvez en proposer un.</small>
            </h3>

            <form action="index.php" method="post" class="needs-validation" novalidate>
                <div class="form-group">
                    <label>Mot proposé</label>
                    <input type="text" class="form-control" name="mot" placeholder="Saisissez votre proposition de mot" required>
                </div>
                <button type="submit" class="btn btn-primary submit">Proposer</button>
            </form>

        </div>
    </div>


    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var forms = document.getElementsByClassName('needs-validation');
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

</body>

</html>
