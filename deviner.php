<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jeu</title>

    <!-- jQuery -->
    <script src="./lib/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Our files -->
    <link rel="stylesheet" href="./styles.css">
    <script src="./script.js"></script>
</head>

<body onload="onLoad()" class="container">

<center><h1><?php echo $_SESSION["pseudo"] ?>, devine un mot !</h1></center>

<div class="card gradient-card">
    <div class="card-body">
        <h3 class="card-title">Le jeu du pendu</h3>
        <p class="card-text">
            Tentez de deviner le mot secret en entrant des lettres une par une au clavier.
            Ne gaspillez pas vos coups, car si trop de vos choix sont erronés vous tuerez
            le pendu et vous perdrez la partie.
        </p>

        <form action="index.php" method="post" >
            <input type="hidden" name="action" value="quit">
            <button type="submit" style="margin-left: 4px" class="btn btn-primary submit">Se déconnecter</button>
        </form>

        <form action="index.php" method="post" >
            <input type="hidden" name="action" value="reset">
            <button type="submit" class="btn btn-primary submit">Réinitialiser</button>
        </form>
    </div>
</div>

<div class="card gradient-card">
    <div class="card-body">
        <h3 class="card-title">
            Devinez le mot<br>
            <small class="text-muted">Voici la forme du mot à deviner</small>
        </h3>
        <h4 class="remaining">
            <small class="text-muted circle-sketch-highlight">Essais restants : <?php echo ($redis->get('essaies'));?></small>
        </h4>

        <img
                style="width: 40%; margin: 20px auto 20px auto; border-radius: 10px; display: block"
                src="./img/<?php echo $redis->get('essaies') ?>.jpeg"
        />


        <input type="text" disabled value=<?php echo '"' . $redis->get('mot_hidden') . '"' ?> id="theword">

        <form action="index.php" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <label>Votre lettre</label>
                <input type="text" maxlength="1" minlength="1" class="form-control" name="lettre" placeholder="Saisissez votre proposition de lettre" required>
            </div>
            <button type="submit" class="btn btn-primary submit" id="letter-submit">Proposer</button>
        </form>


        <table class="table">
            <tbody>
                <tr>
                    <th scope="row">Joueurs</th>
                    <?php
                    foreach ($redis->lrange("pseudo-list", 0 ,10) as $value){
                        echo "<td>".$value."</td>";
                    }
                    ?>
                </tr>
                <tr>
                    <th scope="row">Lettres déjà choisies</th>
                    <?php
                    foreach ($redis->lrange("essaies-list", 0 ,10) as $value){
                        echo "<td>".$value."</td>";
                    }
                    ?>
                </tr>
            </tbody>
        </table>

    </div>
</div>

<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

</body>

</html>
