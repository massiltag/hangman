<link rel="stylesheet" href="styles.css">

<?php

session_start();

require "predis/autoload.php";
Predis\Autoloader::register();


// Connexion à Redis
try {
    $redis = new Predis\Client(array(
        "scheme" => "tcp",
        "host" => "127.0.0.1",//changer le nom de la base
        "port" => 6379,
    ));
}
catch (Exception $e) {
    die($e->getMessage());
}

// Ajout de l'utilisateur dans la base
if(isset($_POST["pseudo"])){
    if(!isset($_SESSION["pseudo"])) {
        $_SESSION["pseudo"] = $_POST["pseudo"];
        $redis->set('pseudo', $_POST["pseudo"]);
        $pseudo = $redis->get('pseudo');
        $redis->rpush("pseudo-list", $pseudo);
        $redis->lrange("pseudo-list", 0 ,10);
    }
}

// Vérifier si l'utilisateur est toujours dans la liste des joueurs
if(isset($_SESSION['pseudo'])) {
    if(!in_array($_SESSION['pseudo'], $redis->lrange("pseudo-list", 0 ,10), false) or $redis->exists('pseudo-list') == false) {
        $deconnexion = true;
    }
}



// Déconnexion
if(isset($_POST["action"]) or isset($deconnexion)) {
    if (isset($_POST["action"])){
        if($_POST["action"] == 'quit') {
            $redis->lrem('pseudo-list', 1, $_SESSION['pseudo']);
        }
        else if($_POST["action"] == 'reset'){
            $redis->del('pseudo-list');
            $redis->del('status');
        }
    }
    unset($_POST);
    session_destroy();
    $quit = true;
}


// Logique concernant le mot proposé
if(isset($_POST["mot"]) && $redis->exists('mot') == false && !isset($quit)) {
    $redis->set('mot', strtoupper($_POST["mot"]));
    $redis->set('mot_hidden', preg_replace("/[^\" \"]/", "_", $redis->get('mot')));
    $redis->set('status', 0);
    $redis->expire('mot', 60);
    $redis->expire('mot_hidden', 60);
}


// Logique concernant la lettre choisie
if(isset($_POST["lettre"]) && $redis->exists('mot') && !isset($quit)) {
    // Vérifier si la lettre n'a pas été choisie
    $exists = false;
    foreach ($redis->lrange("essaies-list", 0 ,10) as $value){
        if (strtolower($_POST["lettre"]) == strtolower($value)) {
            $exists = true;
            break;
        }
    }

    if (!$exists) {
        // Remplacer dans le mot hidden les lettres aux positions correspondantes
        $mot_hidden = $redis->get('mot_hidden');
        $array = str_split($redis->get('mot'));
        $found = false;
        $cpt = 0;
        foreach ($array as $char) {
            if (strtolower($_POST["lettre"]) == strtolower($char)) {
                $found = true;
                $mot_hidden = substr_replace($mot_hidden, $char, $cpt, 1);
            }
            $cpt++;
        }

        if ($found) {    // Good guess
            $redis->set('mot_hidden', $mot_hidden);
            if (!strcontains($mot_hidden, "_")) {
                $redis->del('mot');
                $redis->set('status', '1');
            }
        } else {         // Bad guess
            $redis->decr('essaies');
            if($redis->get('essaies') == 0) {
                $redis->del('mot');
                $redis->set('status', '-1');
            }
        }
        $redis->rpush('essaies-list', $_POST["lettre"]);
    }

}


// Implémenté car pas dispo en PHP7
function strcontains($str, $letter) {
    $array = str_split($str);
    foreach ($array as $char) {
        if (strtolower($letter) == strtolower($char)) {
            return true;
        }
    }
    return false;
}

// Page affichée
if(!isset($_SESSION["pseudo"]) or isset($quit)) {
    include 'pseudo.php';
}
else {
    if($redis->exists('mot')) {
        include 'deviner.php';
    }
    else {
        $redis->set('essaies', 10);
        $redis->del('essaies-list');
        include 'proposer.php';
    }
}



